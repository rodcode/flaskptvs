import flask

def page_not_found():
    """Renders error page."""
    return 'Poll does not exist.', 404
