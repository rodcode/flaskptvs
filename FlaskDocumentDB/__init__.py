"""
The flask application package.
"""

from flask import Flask
from FlaskDocumentDB import views
from FlaskDocumentDB import error_handlers
from FlaskDocumentDB.domain.models import PollNotFound
app = Flask(__name__)

app.add_url_rule('/', view_func=views.home)
app.add_url_rule('/home', view_func=views.home)
app.add_url_rule('/contact', view_func=views.contact)
app.add_url_rule('/about', view_func=views.about)
app.add_url_rule('/seed', view_func=views.seed, methods=['POST'])
app.add_url_rule('/results/<key>', view_func=views.results)
app.add_url_rule('/poll/<key>', view_func=views.details, methods=['GET', 'POST'])
app.register_error_handler(PollNotFound,error_handlers.page_not_found())
