"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, redirect, request
from FlaskDocumentDB.repositories.factory import Factory
from FlaskDocumentDB.settings import REPOSITORY_SETTINGS, REPOSITORY

repository = Factory.create_repository(REPOSITORY, REPOSITORY_SETTINGS)

def home():
    """Renders the home page, with a list of all polls."""

    return render_template(
        'index.html',
        title='Polls',
        year=datetime.now().year,
        polls=repository.get_polls(),
    )

def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
    )

def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        repository_name=REPOSITORY,
    )


def seed():
    """Seeds the database with sample polls."""
    repository.add_sample_polls()
    return redirect('/')


def results(key):
    """Renders the results page."""
    poll = repository.get_poll(key)
    poll.calculate_stats()
    return render_template(
        'results.html',
        title='Results',
        year=datetime.now().year,
        poll=poll,
    )


def details(key):
    """Renders the poll details page."""
    error_message = ''
    if request.method == 'POST':
        try:
            choice_key = request.form['choice']
            repository.increment_vote(key, choice_key)
            return redirect('/results/{0}'.format(key))
        except KeyError:
            error_message = 'Please make a selection.'

    return render_template(
        'details.html',
        title='Poll',
        year=datetime.now().year,
        poll=repository.get_poll(key),
        error_message=error_message,
    )
