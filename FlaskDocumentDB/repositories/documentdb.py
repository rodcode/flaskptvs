
from FlaskDocumentDB.documentdb_client.client_connection import ConnectionDDB
from FlaskDocumentDB.repositories.repository import Repository
from FlaskDocumentDB.domain.models import Poll, Choice, PollNotFound
from FlaskDocumentDB.load_data import _load_samples_json

class DocumentDBRepository(Repository):

    def __init__(self, settings):
        self.__host = settings['DOCUMENTDB_HOST']
        self.__key = settings['DOCUMENTDB_KEY']
        self.__col_link_connection = 'dbs/{0}/colls/users_polls' \
            .format(settings['DOCUMENTDB_DATABASE'])

    @staticmethod
    def _poll_from_doc(doc):
        """Creates a poll object from the DocumentDB poll document."""
        return Poll(str(doc['id']), doc['text'])
    @staticmethod
    def _choice_from_doc(doc):
        """Creates a choice object from the DocumentDB choice subdocument."""
        return Choice(str(doc['id']), doc['text'], doc['votes'])

    def get_polls(self):
        with ConnectionDDB(self.__host, self.__key) as client:
            items = client.QueryDocuments(self.__col_link_connection, 'SELECT * FROM r')
            docs = items.fetch_items()
            polls = [self._poll_from_doc(doc) for doc in docs]
            return polls

    def get_poll(self, poll_key):
        """Returns a poll from the repository."""

        with ConnectionDDB(self.__host, self.__key) as client:
            items = list(client.QueryDocuments(
                self.__col_link_connection,
                {
                    "query": "SELECT * FROM r WHERE r.id=@id",
                    "parameters": [
                        {"name":"@id", "value": poll_key}
                    ]
                }
            ))
            doc = next(iter(items), None)
            if doc is None:
                raise PollNotFound()

            poll = self._poll_from_doc(doc)
            poll.choices = [self._choice_from_doc(choice_doc)
                            for choice_doc in doc['choices']]
            return poll


    def increment_vote(self, poll_key, choice_key):
        """Increment the choice vote count for the specified poll."""
        with ConnectionDDB(self.__host, self.__key) as client:

            items = list(client.QueryDocuments(
                self.__col_link_connection,
                {
                    "query": """SELECT r.id, r.text, r.choices FROM r
                     JOIN c IN r.choices WHERE r.id=@id and c.id=@key_choice""",
                    "parameters": [
                        {"name":"@id", "value": poll_key},
                        {"name":"@key_choice", "value": int(choice_key)},
                    ]
                }
            ))
            doc = next(iter(items), None)
            if doc is None:
                raise PollNotFound()
            inc_choice = next((choice for choice in doc[u'choices'] if choice[u'id'] == int(choice_key)),None)
            if inc_choice is not None:
                inc_choice[u'votes'] += 1 
            client.UpsertDocument(self.__col_link_connection, doc)

    def add_sample_polls(self):
        """Adds a set of polls from data stored in a samples.json file."""
        for sample_poll in _load_samples_json():
            choices = []
            choice_id = 0
            for sample_choice in sample_poll['choices']:
                choice_doc = {
                    'id': choice_id,
                    'text': sample_choice,
                    'votes': 0,
                }
                choice_id += 1
                choices.append(choice_doc)

            poll_doc = {
                'text': sample_poll['text'],
                'choices': choices,
            }
            with ConnectionDDB(self.__host, self.__key) as client:
                client.CreateDocument(self.__col_link_connection, poll_doc)
