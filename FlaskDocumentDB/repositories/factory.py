"""
Factory for the different types of repositories.
"""

from memory import MemoryRepository
from documentdb import DocumentDBRepository

class Factory(object):

    factories = {
        'memory': MemoryRepository,
        'documentdb': DocumentDBRepository
    }

    def __init__(self):
        pass

    @classmethod
    def create_repository(cls, name, settings):

        if name in Factory.factories:
            return Factory.factories[name](settings[name])
        else:
            raise ValueError('Unknown repository.')
