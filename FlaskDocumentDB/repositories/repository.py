"""
Package for the repository
"""

from abc import ABCMeta, abstractmethod

class Repository(object):
    __metaclass = ABCMeta

    @abstractmethod
    def get_polls(self):
        pass
    @abstractmethod
    def get_poll(self, poll_key):
        pass
    @abstractmethod
    def increment_vote(self, poll_key, choice_key):
        pass
    @abstractmethod
    def add_sample_polls(self):
        pass
