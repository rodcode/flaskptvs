import pydocumentdb.document_client as document_client

class ConnectionDDB(object):

    def __init__(self, host, key):
        self.obj = document_client.DocumentClient(host, {'masterKey': key})

    def __enter__(self):
        return self.obj

    def __exit__(self, exception_type, exception_val, trace):
        # extra cleanup in here
        self.obj = None
